package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

type gitlabRoute int

const (
	gitlabListTags gitlabRoute = iota
	gitlabCreateTag
	gitlabRemoveTag
)

type gitlabRoutes map[gitlabRoute]string

type HttpClient interface {
	Do(req *http.Request) (*http.Response, error)
}

type Repository interface {
	ListTags() (interface{}, error)
	CreateTag(projectID, tag string, branch ...string) error
	RemoveTag(projectID, tag string) error
}

type GitlabCommit struct {
	CreatedAt   string `json:"created_at"`
	AuthorEmail string `json:"author_email"`
}

type GitlabRelease struct {
	TagName     string `json:"tag_name"`
	Description string `json:"description"`
}

type GitlabTag struct {
	Commit  GitlabCommit  `json:"commit"`
	Release GitlabRelease `json:"release"`
}

type GitlabClient struct {
	BaseURL      string
	PrivateToken string
	httpClient   HttpClient
	routes       gitlabRoutes
}

func createGitlabRoutes() gitlabRoutes {
	return gitlabRoutes{
		gitlabListTags:  "projects/%s/repository/tags",
		gitlabCreateTag: "projects/%s/repository/tags",
		gitlabRemoveTag: "/projects/%s/repository/tags/%s",
	}
}

func NewGitlabClient(privateToken string) GitlabClient {
	return GitlabClient{
		BaseURL:      "https://gitlab.com/api/v4",
		PrivateToken: privateToken,
		httpClient:   &http.Client{Timeout: 5 * time.Second},
		routes:       createGitlabRoutes(),
	}
}

func (c GitlabClient) makeRequest(req *http.Request) (*http.Response, error) {
	req.Header.Set("Private-Token", c.PrivateToken)
	return c.httpClient.Do(req)
}

func (c GitlabClient) ListTags(projectID string) ([]GitlabTag, error) {
	path := fmt.Sprintf(c.routes[gitlabListTags], projectID)
	url := fmt.Sprintf("%s/%s", c.BaseURL, path)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	resp, err := c.makeRequest(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	tags := []GitlabTag{}
	if err = json.NewDecoder(resp.Body).Decode(&tags); err != nil {
		return nil, err
	}

	return tags, nil
}

func getBranchName(opt []string) string {
	if len(opt) != 0 {
		return opt[0]
	}

	return "master"
}

func (c GitlabClient) CreateTag(projectID, tag string, branch ...string) error {
	path := fmt.Sprintf(c.routes[gitlabCreateTag], projectID)
	url := fmt.Sprintf("%s/%s", c.BaseURL, path)

	req, err := http.NewRequest("POST", url, nil)
	if err != nil {
		return err
	}

	c.injectParams(req, map[string]string{
		"id":       projectID,
		"tag_name": tag,
		"ref":      getBranchName(branch),
		"release_description": fmt.Sprintf(
			"motoboy: Release version %v",
			tag,
		),
	})

	resp, err := c.makeRequest(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil
}

func (c GitlabClient) injectParams(req *http.Request, params map[string]string) {
	q := req.URL.Query()

	for k, v := range params {
			q.Add(k, v)
	}

	req.URL.RawQuery = q.Encode()
}

func (c GitlabClient) RemoveTag(projectID, tag string) error {
	path := fmt.Sprintf(c.routes[gitlabRemoveTag], projectID, tag)
	url := fmt.Sprintf("%s/%s", c.BaseURL, path)

	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		return err
	}

	c.injectParams(req, map[string]string{
		"id":       projectID,
		"tag_name": tag,
	})

	resp, err := c.makeRequest(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	debugRespBody(resp)

	return nil
}

func debugRespBody(resp *http.Response) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)
	b := buf.String()

	fmt.Println(b)
}
