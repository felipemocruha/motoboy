package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/golang/glog"
	"gopkg.in/yaml.v2"
)

type Registry interface {
}

type Cluster interface {
}

type DeployManager struct {
	HookSecretToken string
}

type Config struct {
	GitlabPrivateToken string `yaml:"gitlab-private-token"`
}

func getenv(variable, fallback string) string {
	if val := os.Getenv(variable); len(val) != 0 {
		return val
	} else {
		return fallback
	}
}

func LoadConfig() Config {
	config := Config{}
	configPath := getenv("CONFIG_PATH", "config/config.yaml")

	data, err := ioutil.ReadFile(configPath)
	if err != nil {
		glog.Fatalf("failed to load config: %v", err)
	}

	if err = yaml.Unmarshal(data, &config); err != nil {
		glog.Fatalf("failed to load config: %v", err)
	}

	return config
}

func main() {
	config := LoadConfig()
	gcli := NewGitlabClient(config.GitlabPrivateToken)

	if err := gcli.CreateTag("10097453", "test"); err != nil {
		fmt.Println(err)
		return
	}
	
	tags, err := gcli.ListTags("10097453")
	if err != nil {
		fmt.Println(err)
		return
	}

	if err := gcli.RemoveTag("10097453", "test"); err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(tags)
}
